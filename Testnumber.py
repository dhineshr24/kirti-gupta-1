from unittest.mock import MagicMock

import pytest
from reverseNum import reverse


class Test_reverse_class:
    @pytest.fixture(scope="function", autouse=True)
    def setup(self):
        print("\n Setup Function ")
        yield
        print("\n Teardown Function ")
    @pytest.fixture()
    def test_instantiate_reverse(self):
        rev_no_value = reverse()
        assert rev_no_value.reversDigits()==1
        return rev_no_value

    def test_one_digit_number(self):
        for i in range(0,10):
            rev= reverse(i)
            assert rev.reversDigits()==i

    def test_more_then_one_digit_not_contain_zero_at_end(self):
        rev_two_digit=reverse(56)
        rev_three_digit=reverse(654)
        rev_four_digit=reverse(8765)
        assert rev_two_digit.reversDigits()==65
        assert rev_three_digit.reversDigits()==456
        assert rev_four_digit.reversDigits()==5678

    def test_number_contain_zero_at_the_end(self):
        rev_two_digit=reverse(90)
        rev_three_digit=reverse(170)
        rev_four_digit=reverse(8900)
        assert rev_two_digit.reversDigits() == 9
        assert rev_three_digit.reversDigits() == 71
        assert rev_four_digit.reversDigits() == 98

    def test_returns_output(self, test_instantiate_reverse, monkeypatch):
        mock_file = MagicMock()
        # expected return value
        mock_file.readline = MagicMock(return_value=15)
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = test_instantiate_reverse.readFromFile("input")
        mock_open.assert_called_once_with("input", "r")
        # checking the assert conditions after mocking
        test_instantiate_reverse.add_num(result)
        assert 51 == test_instantiate_reverse.reversDigits()






